require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board
  attr_reader :player1, :player2, :current_player
  def initialize(player1, player2)
    @board = Board.new()
    @player1 = player1
    @player2 = player2
    @player1.mark = :X
    @player2.mark = :O
    @current_player = @player1
  end

  def play_turn
    @current_player.display(@board)
    until @board.over?
      each_turn
    end

    if winner
     winner.display(board)
     puts "#{winner.name} won the game!"
   else
     puts "Tied. Try playing the game again :)"
   end

  end

  def each_turn
    @board.place_mark(@current_player.get_move , @current_player.mark)
    switch_players!
    @current_player.display(board)
  end

  def switch_players!
    @current_player == @player1 ? @current_player = @player2 : @current_player = @player1
  end

  def winner
    return player1 if board.winner == player1.mark
    return player2 if board.winner == player2.mark
    false
  end
end
