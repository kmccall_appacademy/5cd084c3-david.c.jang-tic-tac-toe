class Board
  attr_accessor :grid

  def initialize(grid=Array.new(3){Array.new(3)})
    @grid = grid
  end

  def place_mark(position, mark)
    @grid[position[0]][position[1]] = mark if empty?(position)
  end

  def empty?(position)
    @grid[position[0]][position[1]].nil?
  end

  def winner
    tmp = []
    #row winner
    @grid.each do |row|
      return row[0] if all_same?(row)
    end

    #column winner
    (3).times do |indx|
      tmp = []
      (3).times do |indx2|
        tmp << @grid[indx2][indx]
      end
      return tmp[indx] if all_same?(tmp)
    end
    #diagonal winner
    return @grid[0][0] if all_same?([@grid[0][0], @grid[1][1], @grid[2][2]])
    return @grid[0][2] if all_same?([@grid[0][2], @grid[1][1], @grid[2][0]])

  end

  # helper to determine if all three of the input elemts is the same (but not nil)
  def all_same?(three_element_array)
    return false if three_element_array.uniq[0].nil?
    return true if three_element_array.uniq.size == 1
  end

  # over when board is full or if there is a winner
  # just has to return "truthy" for the test, not true
  def over?
    @grid.flatten.none? { |spot| spot.nil? } || winner
  end

  def [](coordinates)
    @grid[coordinates[0]][coordinates[1]]
  end

  # displays board in a more intuitive way
  def display
    @grid.each { |row| p row}
  end
end
