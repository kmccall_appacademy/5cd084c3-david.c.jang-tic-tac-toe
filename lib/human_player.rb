class HumanPlayer
  attr_reader :name
  attr_accessor :mark, :board
  def initialize(name)
    @name = name
  end

  def get_move
    puts "Please enter a coordinate where you want to leave your mark."
    input = valid(gets.chomp)
    until input && @board.empty?(input)
      if !input
        puts "Incorrect syntax for the coordinate, please enter again."
        puts "Correct syntax is '(0-2), (0-2)'"
      else
        puts "This position is already occupied, please enter another."
        display(board)
      end
      input = valid(gets.chomp)
    end
    # Saved from reg ex usage
    input

  end

  def valid(input)
    input.match(/(\d), (\d)/)
    [$1.to_i, $2.to_i]
  end

  def display(board)
    @board = board
    board.display
  end

end
