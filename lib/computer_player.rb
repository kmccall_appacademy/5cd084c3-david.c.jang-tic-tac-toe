class ComputerPlayer
  attr_reader :name
  attr_accessor :board, :mark

  def initialize(name)
    @name = name
  end

# This method assumes that display method was called previously
# which updates the board (that's how it was written in the specs)
# Also assumes that it won't be run when the board is full
  def get_move
    ans = false
    tmp = []
    #row winner
    (3).times do |indx|
      return ans if ans = winning_move([[indx, 0], [indx, 1], [indx, 2]])
    end

    #column winner
    (3).times do |indx|
      tmp = []
      (3).times do |indx2|
        tmp << [indx2, indx]
      end
      return ans if ans = winning_move(tmp)
    end
    #diagonal winner
    diagonals = [[0, 0], [1, 1], [2, 2] ], [[0, 2], [1, 1], [2, 0]]
    return ans if ans = winning_move(diagonals[0])
    return ans if ans = winning_move(diagonals[1])

    # no winning move for either player, selects a random nil spot
    available_spots = []
    (3).times do |indx|
      (3).times do |indx2|
        available_spots << [indx, indx2] if @board[[indx,indx2]].nil?
      end
    end
    # picks one random coordinate that is open
    available_spots.sample
  end

  # Two of the three have the same mark
  # (computer = win, player = counter their win) and third is nil.
  # My implementation counters the player from winning,
  # but with a priority of winning first.
  def winning_move(three_in_a_row)
    values = three_in_a_row.map { |(x, y)| @board[[x,y]] }
    if values.uniq.size == 2
      values.count(nil) == 1 ? three_in_a_row[values.index(nil)] : false
    else
      false
    end
  end

  def display(board)
    @board = board
  end

end
